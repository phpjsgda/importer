<?php

use Sda\Importer\Importer;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Importer();
$app->run();